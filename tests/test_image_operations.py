import numpy as np
import pytest
from ida_tools.image_operations import auto_contrast, interpolate_image, get_threshold


@pytest.fixture
def sample_image():
    """Fixture to create a simple test image."""
    return np.array([[50, 200, 50], [200, 50, 200], [50, 200, 50]])


def test_auto_contrast_2d():
    image_2d = np.random.rand(100, 100)
    adjusted_image = auto_contrast(image_2d)
    assert adjusted_image.shape == image_2d.shape
    assert adjusted_image.dtype == image_2d.dtype


def test_auto_contrast_3d():
    image_3d = np.random.rand(100, 100, 3)
    adjusted_image = auto_contrast(image_3d)
    assert adjusted_image.shape == image_3d.shape
    assert adjusted_image.dtype == image_3d.dtype


def test_interpolate_image_upsample():
    im = np.random.rand(100, 100)
    result = interpolate_image(im, (2, 2))
    assert result.shape == (200, 200)


def test_interpolate_image_downsample():
    im = np.random.rand(100, 100)
    result = interpolate_image(im, (0.5, 0.5), is_downsample=True)
    assert result.shape == (50, 50)


def test_interpolate_image_extra_factor():
    im = np.random.rand(100, 100)
    result = interpolate_image(im, (2, 2), extra_factor=0.5)
    assert result.shape == (100, 100)


def test_interpolate_image_gaussian_filter_applied():
    im = np.random.rand(100, 100)
    result_without_filter = interpolate_image(im, (0.5, 0.5))
    result_with_filter = interpolate_image(im, (0.5, 0.5), is_downsample=True)
    assert not np.array_equal(result_without_filter, result_with_filter)


def test_interpolate_image_invalid_zoom_factor():
    im = np.random.rand(100, 100)
    with pytest.raises(ValueError):
        interpolate_image(im, (-1, 2))


def test_interpolate_image_invalid_extra_factor():
    im = np.random.rand(100, 100)
    with pytest.raises(ValueError):
        interpolate_image(im, (1, 2), extra_factor=-1)


def test_interpolate_image_different_order_interpolation():
    im = np.random.rand(100, 100)
    result_order_0 = interpolate_image(im, (2, 2), order=0)
    result_order_1 = interpolate_image(im, (2, 2), order=1)
    assert not np.array_equal(result_order_0, result_order_1)


def test_interpolate_image_3d_image():
    im = np.random.rand(10, 10, 10)
    result = interpolate_image(im, (2, 2, 2))
    assert result.shape == (20, 20, 20)


@pytest.mark.parametrize("method", ['li', 'otsu', 'triangle', 'yen'])
def test_threshold_methods(sample_image, method):
    """Test if the threshold methods return the correct type."""
    threshold = get_threshold(sample_image, method)
    assert isinstance(threshold, (int, float, np.integer)), f"Threshold for method {method} should be a numeric type"


def test_invalid_method(sample_image):
    """Test if an invalid method raises a ValueError."""
    with pytest.raises(ValueError):
        get_threshold(sample_image, 'invalid')
