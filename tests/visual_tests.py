import matplotlib.pyplot as plt
from skimage import data
from ida_tools.image_operations import get_threshold, make_binary


def test_get_threshold_and_make_binary():

    # Load an example image
    image = data.camera()  # Convert to grayscale

    threshold = get_threshold(image, 'li')
    binary_image = make_binary(image, threshold)

    # Display results
    fig, axes = plt.subplots(nrows=1, ncols=3, figsize=(10, 3))
    ax = axes.ravel()

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].set_title('Original Image')
    ax[1].hist(image.ravel(), bins=256)
    ax[1].axvline(threshold, color='r', linestyle='--')
    ax[1].set_title('Histogram')
    ax[2].imshow(binary_image, cmap=plt.cm.gray)
    ax[2].set_title('Thresholded Image')

    for a in ax:
        a.axis('off')

    plt.tight_layout()
    plt.show()
