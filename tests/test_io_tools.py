import os
import pytest
from unittest.mock import patch
from argparse import Namespace
from ida_tools.io_utils import create_result_dir, save_args_to_file


@pytest.fixture
def args_object():
    return Namespace(foo='bar', baz='qux')


@pytest.fixture
def temp_directory(tmp_path):
    # Use pytest's tmp_path fixture to create a temporary directory
    return tmp_path


def test_create_result_dir():
    base_path = "/tmp"  # Use a temporary directory for testing
    dir_prefix = "test"

    result_path = create_result_dir(base_path, dir_prefix)

    assert os.path.isdir(result_path)
    assert result_path.startswith(os.path.join(base_path, dir_prefix + "_results_"))

    os.rmdir(result_path)


def test_save_args_to_file_success(args_object, temp_directory):
    filename = "test_arguments.txt"
    save_args_to_file(args_object, temp_directory, filename)
    file_path = temp_directory / filename
    assert os.path.isfile(file_path)
    with open(file_path) as file:
        contents = file.read()
    assert "foo: bar\nbaz: qux\n" == contents


def test_save_args_to_file_nonexistent_directory(args_object):
    with pytest.raises(FileNotFoundError):
        save_args_to_file(args_object, 'non_existent_directory')
