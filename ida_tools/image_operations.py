from scipy.ndimage import zoom, gaussian_filter
from skimage import exposure
from skimage.filters import threshold_triangle, threshold_otsu, threshold_li, threshold_yen
import logging
import numpy as np


def log_image_properties(image):
    logging.info(f'image shape: {image.shape}')
    logging.info(f'image data type: {image.dtype}')
    logging.info(f'image range: {np.min(image), np.max(image)}')


def auto_contrast(image):
    """
    Automatically adjusts the contrast of an image based on its histogram - Similar to FIJI auto-contrast.

    This function rescales the intensity values of the image to span the full intensity range, enhancing the
    overall contrast of the image.

    Parameters:
    - image (numpy.ndarray): A 2D or 3D numpy array representing the input image.

    Returns:
    - adjusted_image (numpy.ndarray): The contrast-adjusted image as a numpy array of the same shape and
                                      type as the input image.
    """

    v_min, v_max = np.percentile(image, (0.1, 99.9))
    adjusted_image = exposure.rescale_intensity(image, in_range=(v_min, v_max))

    return adjusted_image


def interpolate_image(im, per_dim_zoom_factor, extra_factor=1, is_downsample=False, order=3):
    """
    Rescales an ND image with optional antialiasing via Gaussian filtering for downsample.

    Parameters:
    - im: np.ndarray, the input image array.
    - per_dim_zoom_factor: tuple of numbers with size `im.ndims`, zoom factors for each dimension.
    - extra_factor: float, additional scaling factor applied to zoom factors.
    - is_downsample: bool, if True, applies Gaussian filter for antialiasing.
    - order: int, the order of the spline interpolation, default is 3 (cubic).

    Returns:
    - np.ndarray: The rescaled image.
    """

    if not all(s > 0 for s in per_dim_zoom_factor) or extra_factor <= 0:
        raise ValueError("Zoom factors and extra_factor must be positive.")

    zoom_factors = [s * extra_factor for s in per_dim_zoom_factor]

    # Apply Gaussian filter for antialiasing if downsample
    if is_downsample:
        sigma = [max(1.0 / f, 0.01) for f in zoom_factors]  # Avoid division by zero or very small numbers
        im = gaussian_filter(im, sigma=sigma)

    sampled_image = zoom(im, zoom_factors, order=order)
    return sampled_image


def get_threshold(image, method):
    """
    Calculates the threshold of an image using the specified method.

    Parameters:
    - image: np.ndarray, the input image.
    - method: str, the thresholding method to use ('li', 'otsu', 'triangle', 'yen').

    Returns:
    - threshold: float, the calculated threshold value.
    """
    flat_image = image.flatten()
    # flat_image = non_zero_pixels.flatten()

    if method == 'li':
        threshold = threshold_li(flat_image)
    elif method == "otsu":
        threshold = threshold_otsu(flat_image)
    elif method == "triangle":
        threshold = threshold_triangle(flat_image)
    elif method == 'yen':
        threshold = threshold_yen(flat_image)
    else:
        raise ValueError("Threshold method must be one of: 'li', 'otsu', 'triangle', 'yen'")

    logging.info(f'Found threshold {threshold}')

    return threshold


def make_binary(im, thr):

    if not isinstance(im, np.ndarray):
        raise ValueError("Input image must be a numpy array.")
    if thr < 0 or thr > np.iinfo(im.dtype).max:
        raise ValueError(f"Threshold must be between 0 and {np.iinfo(im.dtype).max}.")

    thr_image = np.where(im > thr, 255, 0).astype(np.uint8)
    return thr_image
