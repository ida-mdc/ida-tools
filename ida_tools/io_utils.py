import os
from datetime import datetime
import logging
import tifffile as tif
import cv2
from ida_tools import image_operations


#### MISSING TESTS ####
def read_image(image_path, channel_idx=None):

    logging.info(f'Reading image {image_path}')

    if os.path.splitext(image_path)[1] == '.tif':
        image = tif.imread(image_path, key=channel_idx)
    else:
        image = cv2.imread(image_path)
        # Need to handle channel

    if image is None:
        logging.warning(f'Source {image_path} either not an image file or unsupported format - Skipping')
        return None

    logging.info(f'Read {image_path}, Channel: {channel_idx}, image shape: {image.shape}')
    image_operations.log_image_properties(image)

    return image


def write_image(image_path, image):
    tif.imwrite(image_path, image)
    logging.info(f'Saved image - {image_path}')


def create_result_dir(path, dir_prefix):
    # Input validation
    if not path or not isinstance(path, str):
        raise ValueError("The 'path' argument must be a non-empty string.")
    if not dir_prefix or not isinstance(dir_prefix, str):
        raise ValueError("The 'dir_prefix' argument must be a non-empty string.")
    # Ensure the base path exists
    if not os.path.exists(path):
        raise FileNotFoundError(f"The specified path does not exist: {path}")
    # Ensure the base path is a directory
    if not os.path.isdir(path):
        raise NotADirectoryError(f"The specified path is not a directory: {path}")

    now = datetime.now()
    timestamp_str = now.strftime("%Y%m%d_%H%M%S")
    dir_name = f'{dir_prefix}_results_{timestamp_str}'
    result_path = os.path.join(path, dir_name)

    try:
        os.makedirs(result_path)
    except Exception as e:
        raise OSError(f"Failed to create the directory {result_path}: {e}")

    return result_path


def save_args_to_file(args, result_path, filename='arguments.txt'):
    if not os.path.isdir(result_path):
        raise FileNotFoundError(f"The directory {result_path} does not exist.")

    file_path = os.path.join(result_path, filename)
    try:
        with open(file_path, 'w') as file:
            for arg in vars(args):
                file.write(f'{arg}: {getattr(args, arg)}\n')
    except IOError as e:
        print(f"Error writing to file {file_path}: {e}")
