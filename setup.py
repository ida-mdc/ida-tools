from setuptools import setup, find_packages

setup(
    name='ida_tools',
    version='0.1.0',
    description='Common methods for image analysis',
    author='Ella Bahry',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'numpy>=1.21.0',
        'scipy>=1.7.0',
        'scikit-image>=0.18.0',
        'matplotlib>=3.4.0',
        'opencv-python>=4.5.2',
        'tifffile>=2021.7.2',
        'pytest>=6.2.0',
    ],
)
